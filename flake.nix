{
  description = "A Rust project using naersk";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    naersk = {
      url = "github:nix-community/naersk";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };
  };

  outputs = { self, nixpkgs, naersk, flake-utils, rust-overlay }:
    let
      # If you have a workspace and your binary isn't at the root of the
      # repository, you may need to modify this path.
      cargoToml = builtins.fromTOML (builtins.readFile ./Cargo.toml);
      name = cargoToml.package.name;
    in
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            rust-overlay.overlays.default
          ];
        };
        rust = pkgs.rust-bin.stable.latest;
        naersk-lib = naersk.lib.${system}.override {
          cargo = rust.default;
          rustc = rust.default;
        };
        defaultPackage = naersk-lib.buildPackage {
          pname = name;
          root = ./.;
        };
      in
      rec {
        inherit defaultPackage;

        packages = builtins.listToAttrs [{ inherit name; value = defaultPackage; }];

        # Update the `program` to match your binary's name.
        defaultApp = {
          type = "app";
          program = "${defaultPackage}/bin/hello";
        };

        devShell = pkgs.mkShell {
          inputsFrom = [
            defaultPackage
          ];
          buildInputs = with pkgs; [
            rust-analyzer
            (cargo-cross.overrideAttrs (attrs: {
              nativeBuildInputs = attrs.nativeBuildInputs ++ [
                makeWrapper
              ];
              postInstall = ''
                wrapProgram $out/bin/cross --prefix PATH : ${rustup}/bin
              '';
            }))
          ];
          RUST_SRC_PATH = "${rust.rust-src}/lib/rustlib/src/rust/library";
        };
      }
    );
}
